const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const userRoutes = require('./routes/user');
const courseRoutes = require('./routes/course')

//require and configure.env
require('dotenv').config() 

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended:true }));
app.use(cors());

const connectionString = process.env.DB_CONNECTION_STRING;
mongoose.connection.once('open', () => console.log('Now connected to atlas'));
mongoose.connect(connectionString, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
});



//routes
app.use('/api/users', userRoutes);
app.use('/api/courses', courseRoutes);

const port = process.env.PORT;
app.listen(port, () => {console.log(`Listening on port ${port}`)});