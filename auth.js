//create an auth.js file, it will contains the logic for authorization via tokens and create function to create access

const jwt = require('jsonwebtoken');
const secret = 'CourseBookingAPI';

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	//jwet.sign(payload, secretKey, option)
	return jwt.sign(data, secret, {})
}

//add the additional logic to verify and decode the user information
//next passes on the request to the next middleware function/route/
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization
	
	if (typeof token !== 'undefined') {
		//used to get the token only
		//removes the "bearer"
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			//next() passes the request to the next callback function in the route
			//next callback function = (req, res) => {}
			return (err) ? res.send({ auth: 'failed'}) : next()
		})
	}else {
		return res.send({ auth: 'failed'})
	}
}

module.exports.decode = (token) => {
	if (typeof token !== 'undefined') {
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			//{complete: true} grabs both the request header and the payload
			return (err) ? null :jwt.decode(token, { complete: true}).payload
		})
	} else {
		return null
	}
}