const User = require('../model/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const Course = require('../model/Course');

module.exports.emailExists = (params) => {
	return User.find( {email: params.email})
	.then(result => {
		return result.length > 0 ? true : false
	})
}

module.exports.register = (params) => {
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10)
	})

	return user.save().then((user, err) => {
		return (err) ? true : false
	})
}

//enrolling a user //////////////////////////////////////////////////////////////////////////////////////////////////////////////
// module.exports.enroll = (params) => {
// 	let course = new Course({
// 		name: params.name,
// 		description: params.description,
// 		price: params.price,
// 		createdOn: params.createdOn
// 	})

// 	return User.findById().then(({user.enrollments.push}, courseId) => {
// 		return user.save(user, err)
// 		.then( user => {

// 		})
// 	})
// }

//require the authentication logic for 
module.exports.login = (params) => {
	return User.findOne({ email: params.email })
	.then( user => {
		if(user == null) {
			return false
		}
		//compare password received and hashed password
		//return true if vlaues match
		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		//the mongoose toObject method coverts the mongoose into plain javascript object
		//used to show an object representation of mongoose mode
		//mongoose object will have access to .save() method while plain javascript wont
		if (isPasswordMatched) {
			return {accessToken: auth.createAccessToken(
				user.toObject()) }
		} else {
			return false
		}
		console.log(user);
		console.log(user.toObject());
	})
}


//create the controller logic for getting the user information

module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		//re-assign the password to undefined so it wont be retrieved along with other user data
		user.password = undefined
		return user
	})
}

module.exports.enroll = (params) => {
	return User.findById(params.userId)
	.then(user => {
		user.enrollments.push({courseId: params.courseId})
	
	return user.save().then((user,err) => {
		return Course.findById(params.courseId)
		.then(course => {
			course.enrollees.push({ userId: params.userId})

	return course.save().then((user, err) => {
		return (err) ? false : true
				})	
			})
		})
	})
}

//miscellaneous
module.exports.updateDetails = (params) => {

}

module.exports.changePassword = (params) => {
	
}