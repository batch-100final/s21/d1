const express = require('express');
const router = express.Router();
const auth = require('../auth');
const CourseController = require('../controllers/course');

//create a route to add course
router.post('/', auth.verify, (req, res) => {
	CourseController.add(req.body).then(result => res.send(result))
});
//create a route for getting details of a course
router.get('/', (req, res) => {
	CourseController.getAll()
	.then(courses => res.send(courses))
})

//create a route for to get course Id
router.get('/:courseId', (req, res) => {
	const courseId = req.params.courseId;
	CourseController.getAll( {courseId}).then(course => res.send(course))
})

//create a route for updating a course
router.put('/', auth.verify, (req, res) => {
	CourseController.update(req.body).then(result => res.send(result))
})

//
router.delete('/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId;
	CourseController.archive({ courseId}).then(result => res.send(result))
})


module.exports = router;